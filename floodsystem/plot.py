import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime, timedelta
from floodsystem.station import MonitoringStation
from floodsystem.utils import sorted_by_key
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import update_water_levels
from floodsystem.analysis import polyfit

def plot_water_levels(station, dates, levels):
    plt.plot(dates, levels)

    plt.xlabel("Date")
    plt.xticks(rotation = 75)
    plt.ylabel("Water Level (m)")
    plt.title(station.name)

    plt.hlines(station.typical_range[0], dates[0], dates[-1], label = "Typical low", color = (1, 0, 0))
    plt.hlines(station.typical_range[1], dates[0],dates[-1], label = "Typical high", color =(0, 1, 0))

    plt.legend()
    plt.tight_layout()
    plt.show()





def plot_water_level_with_fit(station, dates, levels, p):
    plt.plot(dates, levels)
    t = matplotlib.dates.date2num(dates)
    poly = (polyfit(dates,levels,p)[0])
    tlist= np.linspace(t[0],t[-1],len(dates))

    plt.plot(dates,poly(tlist-t[0]), label = "Best fit line")
    plt.xlabel("Date")
    plt.xticks(rotation = 75)
    plt.ylabel("Water Level (m)")
    plt.title(station.name)

    plt.hlines(station.typical_range[0], dates[0], dates[-1], label = "Typical low", color = (1, 0, 0))
    plt.hlines(station.typical_range[1], dates[0],dates[-1], label = "Typical high", color =(0, 1, 0))


    plt.legend()
    plt.tight_layout()
    plt.show()
    

















