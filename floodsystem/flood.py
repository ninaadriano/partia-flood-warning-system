"""task 2B - asses flood risk by level"""
#implement function rturning list of tuples
#each tuple should hold station with relative level greater than toleranceand the relative level
from floodsystem.station import MonitoringStation
from floodsystem.utils import sorted_by_key
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import update_water_levels
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.analysis import polyfit
from floodsystem.datafetcher import fetch_measure_levels
import matplotlib
import datetime

def stations_level_over_threshold(stations, tol):
    update_water_levels(stations)
    higher_than_tol = []
    dodgy_stations = inconsistent_typical_range_stations(stations)
    for station in stations:
        relative_level = station.relative_water_level()
        if (station in dodgy_stations) or (relative_level == None):
            relative_level = tol
        
        
        if relative_level > tol:
            higher_than_tol.append((station, relative_level))

    higher_than_tol_sorted = sorted_by_key(higher_than_tol, 1, reverse=True)
    return higher_than_tol_sorted



"""task 2C - function to return N stations with highest relative level"""
def stations_highest_relative_level(stations, N):
    update_water_levels(stations)
    dodgy_stations = inconsistent_typical_range_stations(stations)
    station_relative_level = []
    for station in stations:
        relative_level = station.relative_water_level()
        if (station in dodgy_stations) or (relative_level == None):
            relative_level = 0.0
        else:
            station_relative_level.append((station, relative_level))
        

    station_relative_level_sorted = sorted_by_key(station_relative_level, 1, reverse = True)
    N_highest = []
    for i in range(N):
        N_highest.append(station_relative_level_sorted[i])
    return N_highest


def level_of_severity(stations,severity, threshold):

    assign_risk = stations_level_over_threshold(stations, threshold)
    
    for station in assign_risk:
        station[0].warning_level=severity

def rising_or_falling_water_levels (stations):

    rising_water_levels=[]
    falling_water_levels=[]
    steady_water_levels=[]

    for station in stations:
        try:
            #get data
            dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=0.1))
            
            #polyfit
            poly = polyfit(dates,levels,1)[0]

            #slope
            t = matplotlib.dates.date2num(dates)
            recent_water_level= poly(t[-2]-t[0])#the y axis output for the imput of the second to last value t[-2] of the polyfit graph
            day_after_recent_water_level = poly(t[-1]-t[0])#the y axis output of the imput when t[-1] 
            if recent_water_level < day_after_recent_water_level:
                rising_water_levels.append(station.name)    

            elif recent_water_level > day_after_recent_water_level:
                falling_water_levels.append(station.name)
            else:
                steady_water_levels.append(station.name)   
            
        except:
            pass

    return rising_water_levels
