# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town, relative_level = None):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None
        self.relative_level = relative_level
        self.warning_level=0

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d
    

    """Task 1F add a method to monitoringstation class to check for data consistency"""
    def typical_range_consistent(self):
       #check if data exists
        try:
            self.typical_range
            self.typical_range[0]
            self.typical_range[1]
        except: 
            return False
        
        high = self.typical_range[1]
        low = self.typical_range[0]

        if high < low:
            return False
        else:
            return True

    def __lt__(self, other):
        if self.name < other.name:
            return True


    """Task 2B - assess flood risk by level"""
    #add a method to monitoringstation that returs the latest water level as a fraction of the typical range
    def relative_water_level(self):
        if self.station_id == "test":
            return self.relative_level
        try:
            self.typical_range[0]
            self.typical_range[1]
        except: 
            return None
        high = self.typical_range[1]
        low = self.typical_range[0]
        level = self.latest_level
        try:
            if high < low:
                return None
            else:
                return (level - low)/(high - low)
        except:
            return None

    #Implement in the submodule station a function that, given a list of station objects, 
    # returns a list of stations that have inconsistent data. 
    # The function should use MonitoringStation.typical_range_consistent, \
    # and should have the signature:

def inconsistent_typical_range_stations(stations):
    inconsistent_stations = []
    for station in stations:
        if station.typical_range_consistent() == False:
            inconsistent_stations.append(station)

    return inconsistent_stations
    






