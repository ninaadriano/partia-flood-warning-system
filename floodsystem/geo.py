# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
import numpy as np 
def haversine(theta):
    """Function which works out the haversine of an angle in radians"""
    return (1 - np.cos(theta))/2

def spherical_distance(a, b):
    """ Function that returns spherical distance between two coordinates using haversine formula"""
    lat1 = np.radians(a[0])
    lat2 = np.radians(b[0]) 
    long1 = np.radians(a[1])
    long2 = np.radians(b[1])
    R_earth = 6378.137
    hav_formula = haversine(lat2 - lat1) + np.cos(lat1)*np.cos(lat2)*haversine(long2 - long1)
    return 2*R_earth*np.arcsin(np.sqrt(hav_formula))

def stations_by_distance(stations, p):
    """Function to return list of (station, distance) tuples"""
    station_distance = []
    for station in stations:
        distance = spherical_distance(station.coord, p)
        station_distance.append((station.name, station.town, distance))
    station_distance_sorted = sorted_by_key(station_distance, 2)
    return station_distance_sorted


#ellie can you do 1C in this gap?

def stations_within_radius(stations, centre, r):
    stations_in_radius = []
    for station in stations:
        distance = spherical_distance(station.coord, centre)
        if(distance < r):
            stations_in_radius.append(station)
    return stations_in_radius



"""Task 1D - rivers with stations"""
def rivers_with_stations(stations):
    river_list = []
    for station in stations:
        river = station.river
        if river not in river_list:
            river_list.append(river)
    return river_list


"""implement a function that returns a (dictionary) that maps river names (the ‘key’) to a list of station objects on a given river."""
def stations_by_river(stations):
    river_names_to_stations = {}
    river_list= rivers_with_stations(stations)
    
    for river in river_list:
        station_list= []
        for station in stations: 
            if river == station.river:
                station_list.append(station)

        river_names_to_stations[river] = station_list

    return river_names_to_stations
    
"""Task 1E - function to return list of N rivers with greatest number of stations"""
def rivers_by_station_number(stations, N):
    #call funtions to return rivers with stations and stations at the river
    rivers_list = rivers_with_stations(stations)
    stations_list = stations_by_river(stations)
    rivers_station_number = []
    for river in rivers_list:
        rivers_station_number.append((river, len(stations_list[river])))
    rivers_stations_number_sorted = sorted_by_key(rivers_station_number, 1, reverse = True)
    Nth_entry = rivers_stations_number_sorted[:N]
    while rivers_stations_number_sorted[len(Nth_entry)][1] == (Nth_entry[-1])[1]:
        Nth_entry.append(rivers_stations_number_sorted[len(Nth_entry)])
    return Nth_entry
