
#implement a function that given  the water level time history (dates, levels) 
# for a station computes a least-squares fit of 
# a polynomial of degree p to water level data. 
# The function should return a tuple of 
# (i) the polynomial object and 
# (ii) any shift of the time (date) axis (see below). 


import numpy as np
import matplotlib
import matplotlib.pyplot as plt


def polyfit(dates, levels, p):
    t = matplotlib.dates.date2num(dates)
    p_coeff = np.polyfit(t-t[0], levels, p)
    b=t[0]
    poly = np.poly1d(p_coeff)

    return (poly, b)
