from floodsystem.flood import stations_level_over_threshold
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_relative_level

def test_stations_level_over_threshold():
    stations = []
    update_water_levels(stations)
    
    stations.append(MonitoringStation("test", "m_id", "Name1", (0.0,0.0), None, "river", "town", relative_level = 0.3))
    stations.append(MonitoringStation("test", "m_id", "Name2", (0.0,0.0), (0.0, 1.0), "river", "town", relative_level = 1.1))
    stations.append(MonitoringStation("test", "m_id", "Name3", (0.0,0.0), (2.0,1.0), "river", "town", relative_level = 1.1))
    stations.append(MonitoringStation("test", "m_id", "Name4", (0.0,0.0), (0.0, 1.0), "river", "town", relative_level = 0.5))
    x = stations_level_over_threshold(stations, 0.5)

    assert len(x) == 1
    assert x[0] != ("test")
    assert x[0] != ("Name2", 1.1)


def test_stations_highest_relative_level():
    stations = []
    update_water_levels(stations)

    stations.append(MonitoringStation("test", "m_id", "Name1", (0.0,0.0), None, "river", "town", relative_level = 0.3))
    stations.append(MonitoringStation("test", "m_id", "Name2", (0.0,0.0), (0.0, 1.0), "river", "town", relative_level = 1.1))
    stations.append(MonitoringStation("test", "m_id", "Name3", (0.0,0.0), (2.0,1.0), "river", "town", relative_level = 1.1))
    stations.append(MonitoringStation("test", "m_id", "Name4", (0.0,0.0), (0.0, 1.0), "river", "town", relative_level = 0.5))
    stations.append(MonitoringStation("test", "m_id", "Name5", (0.0,0.0), (0.0, 1.0), "river", "town", relative_level = 0.7))
    stations.append(MonitoringStation("test", "m_id", "Name6", (0.0,0.0), (0.0, 3.5), "river", "town", relative_level = 2.9))
    stations.append(MonitoringStation("test", "m_id", "Name7", (0.0,0.0), (0.0, 1.6), "river", "town", relative_level = 1.4))
    stations.append(MonitoringStation("test", "m_id", "Name8", (0.0,0.0), (0.0, 1.7), "river", "town", relative_level = 1.5))
    stations.append(MonitoringStation("test", "m_id", "Name9", (0.0,0.0), (0.0, 8.9), "river", "town", relative_level = 7.8))
    stations.append(MonitoringStation("test", "m_id", "Name10", (0.0,0.0), (0.0, 1.0), "river", "town", relative_level = 2.5))
    x = stations_highest_relative_level(stations, 3)


    assert len(x) == 3
    assert x[0] != ("Name8", 1.5)
    assert x[0] != ("Name9", 7.8)
    assert x[2] != ("Name9", 7.8)
