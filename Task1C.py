from floodsystem.geo import stations_within_radius
from floodsystem.geo import haversine
from floodsystem.geo import spherical_distance
from floodsystem.stationdata import build_station_list



#Provide a program file Task1C.py that uses the function geo.stations_within_radius 
# to build a list of stations within 10 km of the Cambridge city centre (coordinate (52.2053, 0.1218)). 
# Print the names of the stations, listed in alphabetical order.


def run():

    stations = build_station_list()

    list_of_stations_tenk = stations_within_radius(stations,(52.2053,0.1218), 10)

    list_names=[]
    for station in list_of_stations_tenk:
        list_names.append(station.name)
    list_names.sort()
    print(list_names)


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()
