import matplotlib
import matplotlib.pyplot as plt
import datetime 
from datetime import datetime, timedelta
from floodsystem.station import MonitoringStation
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.flood import stations_highest_relative_level
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import level_of_severity
from floodsystem.flood import rising_or_falling_water_levels

def run():

    stations = build_station_list()


    level_of_severity(stations,1,0.7)

    level_of_severity(stations,2,1.2)
    level_of_severity(stations,3,2.5)
    level_of_severity(stations,4,3.5)

    severe_risk_stations=[]
    high_risk_stations=[]
    medium_risk_stations=[]
    low_risk_stations=[]

    stations_to_test_tuple = stations_level_over_threshold(stations,0.7) 
    stations_to_test = []
    for station in stations_to_test_tuple:
        stations_to_test.append(station[0])

    

    for station in stations:
       

        if station.warning_level ==1:
            low_risk_stations.append(station.name)
        if station.warning_level ==2:
            medium_risk_stations.append(station.name)
        if station.warning_level ==3:
            high_risk_stations.append(station.name)
        if station.warning_level ==4:
            severe_risk_stations.append(station.name)

    print("Low risk:", low_risk_stations) 
    print("Medium risk:", medium_risk_stations) 
    print("High risk:", high_risk_stations) 
    print("Servere risk:", severe_risk_stations) 
    print("Finding where the water level is rising")
    rising_stations = rising_or_falling_water_levels(stations_to_test)
    print("Stations with rising water level:", rising_stations) 


if __name__ == "__main__":

    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()