from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def run():
    stations = build_station_list()
    z = rivers_by_station_number(stations, 9)

    print("Rivers with upto 9th highest number of stations:", z)











if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()