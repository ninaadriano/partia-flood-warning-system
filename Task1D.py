from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_stations
from floodsystem.geo import stations_by_river


def run():
    stations = build_station_list()
    y = sorted(rivers_with_stations(stations))
    print("Number of rivers with at least one station:", len(y))
    print("First 10 rivers with station sorted in alphabetical order", y[:10])


    x = stations_by_river(stations)
    
    river_aire_stations = x["River Aire"]
    river_aire_list = []
    for i in river_aire_stations:
        river_aire_list.append(i.name)

    river_aire_list.sort()
    print(river_aire_list)
  
    river_cam_stations = x["River Cam"]
    river_cam_list = []
    for i in river_cam_stations:
        river_cam_list.append(i.name)

    river_cam_list.sort()
    print(river_cam_list)
  
    river_thames_stations = x["River Thames"]
    river_thames_list = []
    for i in river_thames_stations:
        river_thames_list.append(i.name)

    river_thames_list.sort()
    print(river_thames_list)
  


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()


