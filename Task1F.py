from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations



def run():
    stations = build_station_list()
    x = inconsistent_typical_range_stations(stations)
    y = sorted(x)
    print("Staions with inconsistent data:", y)










if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()