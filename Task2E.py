import matplotlib.pyplot as plt
import datetime 
from datetime import datetime, timedelta
from floodsystem.station import MonitoringStation
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_relative_level
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels

def run():
    stations = build_station_list()
    high_stations = stations_highest_relative_level(stations, 5)
    for station in high_stations:
        dates, levels = fetch_measure_levels(station[0].measure_id, dt=timedelta(days=10))
        plot_water_levels(station[0], dates, levels)



if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()