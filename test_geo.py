"""test for the geo module"""
from floodsystem.station import MonitoringStation
from floodsystem.geo import stations_by_distance
from floodsystem.geo import rivers_with_stations
from floodsystem.geo import rivers_by_station_number
from floodsystem.geo import stations_within_radius
from floodsystem.geo import stations_by_river
def test_stations_by_distance():

    #fake list of stations
    stations = []
    stations.append(MonitoringStation("s_id", "m_id", "Name1", (1,0), (0,0), "river", "town"))
    stations.append(MonitoringStation("s_id", "m_id", "Name2", (2,0), (0,0), "river", "town"))
    stations.append(MonitoringStation("s_id", "m_id", "Name3", (3,0), (0,0), "river", "town"))
    stations.append(MonitoringStation("s_id", "m_id", "Name4", (0,1), (0,0), "river", "town"))
    stations.append(MonitoringStation("s_id", "m_id", "Name5", (0,2), (0,0), "river", "town"))
    stations.append(MonitoringStation("s_id", "m_id", "Name6", (0,3), (0,0), "river", "town"))

    s = stations_by_distance(stations, (0,0))
    

    assert len(s) == 6
    assert s[0] == ('Name1', 'town', 111.31949079326246)



def test_rivers_with_stations():

    #fake list of stations
    stations = []
    stations.append(MonitoringStation("s_id1", "m_id1", "Name1", (1,0), (0,0), "river1", "town"))
    stations.append(MonitoringStation("s_id2", "m_id2", "Name2", (2,0), (0,0), "river2", "town"))
    stations.append(MonitoringStation("s_id3", "m_id3", "Name3", (3,0), (0,0), "river3", "town"))
    stations.append(MonitoringStation("s_id4", "m_id4", "Name4", (0,1), (0,0), "river4", "town"))
    stations.append(MonitoringStation("s_id5", "m_id5", "Name5", (0,2), (0,0), "river5", "town"))
    stations.append(MonitoringStation("s_id6", "m_id6", "Name6", (0,3), (0,0), "river5", "town"))

    s = rivers_with_stations(stations)

    assert len(s) == 5
    assert s == ["river1", "river2", "river3", "river4", "river5"]


def test_rivers_by_station_number():
    #fake list of stations
    stations = []
    stations.append(MonitoringStation("s_id1", "m_id1", "Name1", (1,0), (0,0), "river1", "town"))
    stations.append(MonitoringStation("s_id2", "m_id2", "Name2", (2,0), (0,0), "river1", "town"))
    stations.append(MonitoringStation("s_id3", "m_id3", "Name3", (3,0), (0,0), "river1", "town"))

    stations.append(MonitoringStation("s_id4", "m_id4", "Name4", (0,1), (0,0), "river2", "town"))
    stations.append(MonitoringStation("s_id5", "m_id5", "Name5", (0,2), (0,0), "river2", "town"))

    stations.append(MonitoringStation("s_id6", "m_id6", "Name6", (0,3), (0,0), "river3", "town"))
    stations.append(MonitoringStation("s_id7", "m_id1", "Name7", (4,0), (0,0), "river3", "town"))
    stations.append(MonitoringStation("s_id8", "m_id2", "Name8", (5,0), (0,0), "river3", "town"))

    stations.append(MonitoringStation("s_id9", "m_id3", "Name9", (6,0), (0,0), "river4", "town"))
    stations.append(MonitoringStation("s_id10", "m_id4", "Name10", (0,4), (0,0), "river4", "town"))

    stations.append(MonitoringStation("s_id11", "m_id5", "Name11", (0,5), (0,0), "river5", "town"))

    p = rivers_by_station_number(stations, 1)
    return p
    assert len(p) == 5


def test_stations_within_radius():
    stations =[]
    stations.append(MonitoringStation("s_id1", "m_id1", "Name1", (1,0), (0,0), "river1", "town"))
    stations.append(MonitoringStation("s_id2", "m_id2", "Name2", (2,0), (0,0), "river1", "town"))
    stations.append(MonitoringStation("s_id3", "m_id3", "Name3", (3,0), (0,0), "river1", "town"))

    stations.append(MonitoringStation("s_id4", "m_id4", "Name4", (0,1), (0,0), "river2", "town"))
    stations.append(MonitoringStation("s_id5", "m_id5", "Name5", (0,2), (0,0), "river2", "town"))

    stations.append(MonitoringStation("s_id6", "m_id6", "Name6", (0,3), (0,0), "river3", "town"))
    stations.append(MonitoringStation("s_id7", "m_id1", "Name7", (4,0), (0,0), "river3", "town"))
    stations.append(MonitoringStation("s_id8", "m_id2", "Name8", (5,0), (0,0), "river3", "town"))

    stations.append(MonitoringStation("s_id9", "m_id3", "Name9", (6,0), (0,0), "river4", "town"))
    stations.append(MonitoringStation("s_id10", "m_id4", "Name10", (0,4), (0,0), "river4", "town"))

    stations.append(MonitoringStation("s_id11", "m_id5", "Name11", (0,5), (0,0), "river5", "town"))

    y = stations_within_radius(stations,(0,0), 278)
    assert len(y) == 4
    


def test_stations_by_river ():
    stations = []
    stations.append(MonitoringStation("s_id1", "m_id1", "Name1", (1,0), (0,0), "river1", "town"))
    stations.append(MonitoringStation("s_id2", "m_id2", "Name2", (2,0), (0,0), "river2", "town"))
    stations.append(MonitoringStation("s_id3", "m_id3", "Name3", (3,0), (0,0), "river3", "town"))

    stations.append(MonitoringStation("s_id5", "m_id1", "Name1", (1,0), (0,0), "river1", "town"))
    stations.append(MonitoringStation("s_id6", "m_id2", "Name2", (2,0), (0,0), "river2", "town"))

    p = stations_by_river(stations)

    assert len(p["river2"])== 2
    assert len (p["river1"])==2
    assert len (p["river3"])== 1
    assert (p["river3"])[0].station_id == "s_id3"



