from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold
from floodsystem.station import MonitoringStation

def run():
    stations = build_station_list()
    x = stations_level_over_threshold(stations, 0.8)
    #need to print name of station with relative level over 0.8 and what its relative level is
    for a in x:
        print(a[0].name, a[1])










if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()