from floodsystem.geo import haversine
from floodsystem.geo import spherical_distance
from floodsystem.geo import stations_by_distance

from floodsystem.stationdata import build_station_list

"""function to provide list of tuples (station name, town, distance) for closest and furthest stations from Cambridge city centre"""

def run():
    stations = build_station_list()
    x = stations_by_distance(stations, (52.2053, 0.1218)) 
    print("Clostest 10 stations by distance:", x[:10])
    print("Furthest 10 stations by distance:", x[-10:])



if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()