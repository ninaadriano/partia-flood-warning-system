from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_highest_relative_level

def run():
    stations = build_station_list()
    x = stations_highest_relative_level(stations, 10)
    for a in x:
        print(a[0].name, a[1])









if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()